import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private products = [
    {
      id:1,
      name:'Huevo',
      category:'Comida',
      description:'A puro huee',
      price:'1',
    },
    {
      id:2,
      name:'Vino',
      category:'Bebida',
      description:'Un tintillo',
      price:'13',
    },
    {
      id:3,
      name:'Bife',
      category:'Carne',
      description:'beef',
      price:'15',
    },
  ]

  getAllProducts(){
    return this.products;
  }

  getProductById(id: any){
    return this.products.filter((product) => product.id == id);
  }
}
