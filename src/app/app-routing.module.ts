import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DetailsproductComponent } from './products/detailsproduct/detailsproduct.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'products', component: ProductListComponent},
  { path: 'product/:id', component: DetailsproductComponent},
  { path: '**', component: NotfoundComponent},
];
// HomePage
// 404
// DetailsProduct
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
